#!/usr/bin/env bash

# Exit script as soon as a command fails
set -e

# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Parse arguments
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
POSITIONAL_ARGS=()
NEXTCLOUD_INTEGRATION="NO"
DISABLE_SECRETS_MANAGEMENT="NO"
while [[ $# -gt 0 ]]; do
    case $1 in
        --nextcloud-integration)
            NEXTCLOUD_INTEGRATION="YES"
            shift
            ;;
        --disable-secrets-management)
            DISABLE_SECRETS_MANAGEMENT="YES"
            shift
            ;;
        -*)
            echo "Unknown option $1"
            exit 1
            ;;
        *)
            POSITIONAL_ARGS+=("$1") # save positional arg
            shift
            ;;
    esac
done
if (( ${#POSITIONAL_ARGS[@]} != 1 )); then
    echo "You must provide only one argument: a domain (example: 'mydomain.com')"
    echo "Help: $0 [--nextcloud-integration] mydomain.com"
    exit 1
fi
EOLE_DOMAIN=${POSITIONAL_ARGS[0]}
CONF_NAME="env_${EOLE_DOMAIN}"

# Get global config
source config_k8s

# Load common variables
source helpers/load_common_vars.sh

# Fetch EOLE3 code if needed
# We need the code to customize the 'vars.ini' files
if [ -e "$EOLE3_CODE_DIR" ]; then
    echo 'EOLE3 code has already been checkout'
else
    echo "Checkout EOLE3 code"
    git clone -b "$EOLE3_GIT_BRANCH" "$EOLE3_GIT_URL" "$EOLE3_CODE_DIR"
fi

# Fetch secrets manager if needed
if [ -e "$SECRETS_MANAGER_DIR" ]; then
    echo 'Secrets manager has already been checkout'
else
    echo "Checkout secrets manager"
    git clone -b "$SECRETS_MANAGER_GIT_BRANCH" "$SECRETS_MANAGER_GIT_URL" "$SECRETS_MANAGER_DIR"
fi

#
# Generate a default EOLE3 configuration
#

# Initialize a configuration if there is none
echo "Create (if needed) folder '${CONF_DIR}' to store domain's config"
mkdir -p "$CONF_DIR"

#
# Customize cluster-vars.ini & vars.ini
#
echo "Modify configuration into folder '${CONF_DIR}'"
VARS_INI="${CONF_DIR}/vars.ini"
cp "${EOLE3_CODE_DIR}/vars.ini" "$VARS_INI"
CLUSTER_VARS_INI="${CONF_DIR}/cluster-vars.ini"
cp "${EOLE3_CODE_DIR}/cluster-vars.ini" "$CLUSTER_VARS_INI"

# Customize cluster
sed -i '/^\[general\]$/,/^\[/ s/^demoMode=.*$/demoMode=false/' "$CLUSTER_VARS_INI"

# Customize base services
sed -i '/^\[general\]$/,/^\[/ s/^domain=.*$/domain='"$EOLE_DOMAIN"'/' "$VARS_INI"
sed -i '/^\[laboite\]$/,/^\[/ s/^appName=.*$/appName=MyEOLE3/' "$VARS_INI"
sed -i '/^\[laboite\]$/,/^\[/ s/^appDescription=.*$/appDescription=My EOLE3 test instance/' "$VARS_INI"
sed -i '/^\[laboite\]$/,/^\[/ s/^theme=.*$/theme=eole/' "$VARS_INI"
# Parameter 'whiteDomains' is used ! Need to patch "${INSTALL_DIR}/laboite-values.yaml" directly
# sed -i '/^\[laboite\]$/,/^\[/ s/^whiteDomains=.*$/whiteDomains="'"$EOLE_DOMAIN"'"/' "$VARS_INI"
sed -i '/^\[cert-manager\]$/,/^\[/ s/^enabled=.*$/enabled=false/' "$VARS_INI"
sed -i '/^\[mezig\]$/,/^\[/ s/^deploy=.*$/deploy=false/' "$VARS_INI"
sed -i '/^\[agenda\]$/,/^\[/ s/^deploy=.*$/deploy=false/' "$VARS_INI"
sed -i '/^\[sondage\]$/,/^\[/ s/^deploy=.*$/deploy=false/' "$VARS_INI"
sed -i '/^\[lookup-server\]$/,/^\[/ s/^deploy=.*$/deploy=false/' "$VARS_INI"
sed -i '/^\[radicale\]$/,/^\[/ s/^deploy=.*$/deploy=false/' "$VARS_INI"
sed -i '/^\[blog\]$/,/^\[/ s/^deploy=.*$/deploy=false/' "$VARS_INI"
sed -i '/^\[frontnxt\]$/,/^\[/ s/^deploy=.*$/deploy=false/' "$VARS_INI"
sed -i '/^\[nextcloud\]$/,/^\[/ s/^enable=.*$/enable=false/' "$VARS_INI"
# Default value => 8Gi
sed -i '/^\[keycloak\]$/,/^\[/ s/^postgresSize=.*$/postgresSize=4Gi/' "$VARS_INI"
# Default value => 4
sed -i '/^\[minio\]$/,/^\[/ s/^replicaCount=.*$/replicaCount=2/' "$VARS_INI"
# Default value => 5Gi
sed -i '/^\[minio\]$/,/^\[/ s/^replicaVolSize=.*$/replicaVolSize=4Gi/' "$VARS_INI"
# Default value => 5
sed -i '/^\[mongodb\]$/,/^\[/ s/^replicaCount=.*$/replicaCount=2/' "$VARS_INI"
# Default value => 8Gi
sed -i '/^\[mongodb\]$/,/^\[/ s/^replicaVolSize=.*$/replicaVolSize=4Gi/' "$VARS_INI"

if [ "$NEXTCLOUD_INTEGRATION" = "YES" ]; then
    echo "Enable Nextcloud integration"
    sed -i '/^\[frontnxt\]$/,/^\[/ s/^deploy=.*$/deploy=true/' "$VARS_INI"
    sed -i '/^\[nextcloud\]$/,/^\[/ s/^enable=.*$/enable=true/' "$VARS_INI"
    # Parameter 'url' must start with 'http://' or 'https://'
    sed -i '/^\[nextcloud\]$/,/^\[/ s/^url=.*$/url=https:\/\/nextcloud.'"$EOLE_DOMAIN"'/' "$VARS_INI"
    # LaBoite's Nextcloud Group Plugin need the Nextcloud admin username and password
    sed -i '/^\[nextcloud\]$/,/^\[/ s/^nextcloudUser=.*$/nextcloudUser=admin/' "$VARS_INI"
    sed -i '/^\[nextcloud\]$/,/^\[/ s/^nextcloudPassword=.*$/nextcloudPassword=changeme/' "$VARS_INI"
    #FIXME Nextcloud quota - Usage ? Default for every user ?
    # sed -i '/^\[nextcloud\]$/,/^\[/ s/^nextcloudQuota=.*$/nextcloudQuota=1073741824/' "$VARS_INI"
    #FIXME API keys for nextcloud token retrieval - Usage ?
    # sed -i '/^\[nextcloud\]$/,/^\[/ s/^nextcloudApiKeys=.*$/nextcloudApiKeys=une-cle-api-de-ton-choix/' "$VARS_INI"
    #FIXME Password used to get token from sessiontoken app - Usage ?
    # sed -i '/^\[nextcloud\]$/,/^\[/ s/^sessionTokenKey=.*$/sessionTokenKey=MCM/' "$VARS_INI"
    #FIXME Application name given to sessiontoken app - Usage ?
    # sed -i '/^\[nextcloud\]$/,/^\[/ s/^sessionTokenAppName=.*$/sessionTokenAppName=eole3.glaude/' "$VARS_INI"
fi

# Inject secret tokens
if [ "$DISABLE_SECRETS_MANAGEMENT" = "NO" ]; then
    # sed -i '/^\[grafana\]$/,/^\[/ s/^admin-password=.*$/admin-password=@@@@grafana.admin@@@@/' "$CLUSTER_VARS_INI"
    sed -i '/^\[keycloak\]$/,/^\[/ s/^adminPassword=.*$/adminPassword=@@@@keycloak.admin@@@@/' "$VARS_INI"
    sed -i '/^\[keycloak\]$/,/^\[/ s/^adminapiPassword=.*$/adminapiPassword=@@@@laboite.keycloak@@@@/' "$VARS_INI"
    sed -i '/^\[keycloak\]$/,/^\[/ s/^postgresAdminPassword=.*$/postgresAdminPassword=@@@@keycloak.admin.postgres@@@@/' "$VARS_INI"
    sed -i '/^\[keycloak\]$/,/^\[/ s/^postgresPassword=.*$/postgresPassword=@@@@keycloak.user.postgres@@@@/' "$VARS_INI"
    sed -i '/^\[smtp\]$/,/^\[/ s/^password=.*$/password=@@@@smtp.user@@@@/' "$VARS_INI"
    sed -i '/^\[BBB\]$/,/^\[/ s/^secret=.*$/secret=@@@@BBB.secret@@@@/' "$VARS_INI"
    sed -i '/^\[minio\]$/,/^\[/ s/^minioSecret=.*$/minioSecret=@@@@laboite.minio@@@@/' "$VARS_INI"
    sed -i '/^\[minio\]$/,/^\[/ s/^minioRootSecret=.*$/minioRootSecret=@@@@minio.root@@@@/' "$VARS_INI"
    sed -i '/^\[mongodb\]$/,/^\[/ s/^mongoRootPassword=.*$/mongoRootPassword=@@@@mongodb.root@@@@/' "$VARS_INI"
    sed -i '/^\[mongodb\]$/,/^\[/ s/^mongoPassword=.*$/mongoPassword=@@@@laboite.mongodb@@@@/' "$VARS_INI"
    sed -i '/^\[nextcloud\]$/,/^\[/ s/^nextcloudPassword=.*$/nextcloudPassword=@@@@nextcloud.admin@@@@/' "$VARS_INI"
    #FIXME Secret set by us ? Set inside Nextcloud (we get it and inject it here) ?
    # sed -i '/^\[nextcloud\]$/,/^\[/ s/^nextcloudApiKeys=.*$/nextcloudApiKeys=@@@@nextcloud.api.keys@@@@/' "$VARS_INI"
    #FIXME Secret set by us ? Set inside Nextcloud (we get it and inject it here) ?
    # sed -i '/^\[nextcloud\]$/,/^\[/ s/^sessionTokenKey=.*$/sessionTokenKey=@@@@nextcloud.session.token.key@@@@/' "$VARS_INI"
fi

#
# Check if SSL certificate is in place or not
#
if [ -e "${CONF_DIR}/${WILDCARD_SSL_CERT}" ] && [ -e "${CONF_DIR}/${WILDCARD_SSL_KEY}" ]; then
    echo "Wildcard SSL certificate for domain '${EOLE_DOMAIN}' are already in place"
else
    echo
    echo "**WARNING** NOW you need to setup wildcard SSL certificate files for domain '${EOLE_DOMAIN}':"
    echo "  - Copy the wildcard SSL certificate for domain '${EOLE_DOMAIN}' to '${WILDCARD_SSL_CERT}'"
    echo "  - Copy the wildcard SSL key for domain '${EOLE_DOMAIN}' to '${WILDCARD_SSL_KEY}'"
fi
