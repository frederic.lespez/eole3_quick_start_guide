#!/usr/bin/env bash

# Exit script as soon as a command fails
set -e

# Get global config
source config_k8s

KUBECONFIG="$(pwd)/${EOLE3_KUBECONFIG}"
TOOLS_DIR="$(pwd)/bin"

echo "Deploy K8S cluster on '${EOLE3_K8S_HOSTNAME}' with EOLE3 base"
# shellcheck disable=SC2087
ssh -T root@$EOLE3_K8S_HOSTNAME << EOF
    # Avoid "too many open files" errors"
    echo 'fs.inotify.max_user_instances=1280' > /etc/sysctl.d/local.conf
    echo 'fs.inotify.max_user_watches=655360' >> /etc/sysctl.d/local.conf
    sysctl -p /etc/sysctl.d/local.conf
    git clone -b "$EOLE3_BASE_GIT_BRANCH" "$EOLE3_BASE_GIT_URL"
    bash provisionner/install-eolebase3.sh
EOF

echo "Fetch Kubernetes config and make it usable from this machine"
scp "root@${EOLE3_K8S_HOSTNAME}:/root/.kube/config" "$KUBECONFIG"
chmod 600 "$KUBECONFIG"
sed -E -i 's/^(\s+server: https:\/\/).+(:[0-9]+)$/\1'"$EOLE3_K8S_HOSTNAME"'\2/' "$KUBECONFIG"

echo "Try to list all defined namespaces on the K8S cluster to see if everything's fine"
export KUBECONFIG
"${TOOLS_DIR}/kubectl" get namespaces

echo
echo "To easily access the K8S cluster, you can do run this in your terminal"
echo "export KUBECONFIG=${KUBECONFIG}"
