#!/usr/bin/env bash

# Exit script as soon as a command fails
set -e

TOOLS_DIR="$(pwd)/bin"

# curl is not required by EOLE3, but by these scripts :-)
echo "Install some tools from distribution packages (sudo will ask your password)"
sudo apt install curl jq python3-click python3-jinja2

echo "Create folder '${TOOLS_DIR}' to store downloaded tools"
mkdir -p bin

echo "Install latest version of Kubernetes command-line tool"
KUBECTL_LATEST_TAG=$(curl -L -s https://dl.k8s.io/release/stable.txt)
curl -SsL "https://dl.k8s.io/release/${KUBECTL_LATEST_TAG}/bin/linux/amd64/kubectl" -o "${TOOLS_DIR}/kubectl"

echo "Install latest version of HELM"
HELM_LATEST_TAG=$(curl -Ls https://github.com/helm/helm/releases | grep 'href="/helm/helm/releases/tag/v3.[0-9]*.[0-9]*\"' | sed -E 's/.*\/helm\/helm\/releases\/tag\/(v[0-9\.]+)".*/\1/g' | head -1)
curl -SsL "https://get.helm.sh/helm-${HELM_LATEST_TAG}-linux-amd64.tar.gz" | tar xz --directory "$TOOLS_DIR" --strip-components 1 --no-anchored helm

echo "Install latest version of MinIO MC client"
curl -SsL https://dl.min.io/client/mc/release/linux-amd64/mc -o "${TOOLS_DIR}/mc"

# echo "Install latest version of MKCERT"
# curl -SsL https://dl.filippo.io/mkcert/latest?for=linux/amd64 -o "${TOOLS_DIR}/mkcert"

echo "Be sure that all tools are executable"
chmod 750 bin/*

echo
echo "To easily use the tools, you can do run this in your terminal"
echo "export PATH=${TOOLS_DIR}:\$PATH"