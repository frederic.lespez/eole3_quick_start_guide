#!/usr/bin/env bash

# Exit script as soon as a command fails
set -e

# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Parse arguments
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
POSITIONAL_ARGS=()
DISABLE_SECRETS_MANAGEMENT="NO"
while [[ $# -gt 0 ]]; do
    case $1 in
        --disable-secrets-management)
            DISABLE_SECRETS_MANAGEMENT="YES"
            shift
            ;;
        -*)
            echo "Unknown option $1"
            exit 1
            ;;
        *)
            POSITIONAL_ARGS+=("$1") # save positional arg
            shift # past argument
            ;;
    esac
done
if (( ${#POSITIONAL_ARGS[@]} == 0 )); then
    echo "You must provide at least one argument: a folder name containing a configuration (example: 'env_mydomain.com')"
    echo "Help: $0 [--update] env_mydomain.com [addon_name1 [addon_name2 [...]]]"
    exit 1
fi
CONF_NAME=${POSITIONAL_ARGS[0]}
ADDON_LIST=("${POSITIONAL_ARGS[@]:1}")

# Get global config
source config_k8s

# Load common variables
source helpers/load_common_vars.sh

for ADDON in "${ADDON_LIST[@]}"; do
    echo "Modify configuration into folder '${CONF_DIR}' for addon '${ADDON}'"
    ADDON_VARS_INI="${CONF_DIR}/${ADDON}-vars.ini"

    # Customize addon vars.ini
    cp "${EOLE3_CODE_DIR}/addons/${ADDON}/${ADDON}-vars.ini" "$ADDON_VARS_INI"

    # Apply specific addon configuration
    case $ADDON in
        codimd)
            sed -i '/^\[keycloak\]$/,/^\[/ s/^redirectUris=.*$/redirectUris=http:\/\/codimd.'"$EOLE_DOMAIN"'\/*/' "$ADDON_VARS_INI"
            # Inject secret tokens
            if [ "$DISABLE_SECRETS_MANAGEMENT" = "NO" ]; then
                sed -i '/^\[codimd\]$/,/^\[/ s/^adminPassword=.*$/adminPassword=@@@@codimd.admin@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[codimd\]$/,/^\[/ s/^dbpassword=.*$/dbpassword=@@@@codimd.db@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[keycloak\]$/,/^\[/ s/^clientSecret=.*$/clientSecret=@@@@codimd.keycloak@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[minio\]$/,/^\[/ s/^minioSecret=.*$/minioSecret=@@@@codimd.minio@@@@/' "$ADDON_VARS_INI"
            fi
            ;;
        nextcloud)
            sed -i '/^\[nextcloud\]$/,/^\[/ s/^installpersistence=.*$/installpersistence=true/' "$ADDON_VARS_INI"
            sed -i '/^\[nextcloud\]$/,/^\[/ s/^installvolumesize=.*$/installvolumesize=2Gi/' "$ADDON_VARS_INI"
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^redisEnable=.*$/redisEnable=true/' "$ADDON_VARS_INI"
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^redisVolumeSize=.*$/redisVolumeSize=2Gi/' "$ADDON_VARS_INI"
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^redisReplicaCount=.*$/redisReplicaCount=2/' "$ADDON_VARS_INI"

            # Database: internal (SQLite)
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbprovider=.*$/dbprovider=internal/' "$ADDON_VARS_INI"
            # Database: mariadb
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbprovider=.*$/dbprovider=mariadb/' "$ADDON_VARS_INI"
            # Database: postgresql
            sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbprovider=.*$/dbprovider=postgresql/' "$ADDON_VARS_INI"
            # Database: external
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbprovider=.*$/dbprovider=external/' "$ADDON_VARS_INI"
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbexternal=.*$/dbexternal=postgresql/' "$ADDON_VARS_INI"
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbexternalhost=.*$/dbexternalhost=HOSTNAME_OR_IP/' "$ADDON_VARS_INI"
            # sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbexternalport=.*$/dbexternalport=5432/' "$ADDON_VARS_INI"
            # Database: common to all types (except SQLite database)
            sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbpersistence=.*$/dbpersistence=true/' "$ADDON_VARS_INI"
            sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbvolumesize=.*$/dbvolumesize=2Gi/' "$ADDON_VARS_INI"

            # Inject secret tokens
            if [ "$DISABLE_SECRETS_MANAGEMENT" = "NO" ]; then
                sed -i '/^\[nextcloud\]$/,/^\[/ s/^adminPassword=.*$/adminPassword=@@@@nextcloud.admin@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[nextcloud\]$/,/^\[/ s/^dbpassword=.*$/dbpassword=@@@@nextcloud.db@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[nextcloud\]$/,/^\[/ s/^redisAuthPassword=.*$/redisAuthPassword=@@@@nextcloud.redis@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[keycloak\]$/,/^\[/ s/^clientSecret=.*$/clientSecret=@@@@nextcloud.keycloak@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[minio\]$/,/^\[/ s/^minioSecret=.*$/minioSecret=@@@@nextcloud.minio@@@@/' "$ADDON_VARS_INI"
            fi
            ;;
        rocketchat)
            # Inject secret tokens
            if [ "$DISABLE_SECRETS_MANAGEMENT" = "NO" ]; then
                sed -i '/^\[rocketchat\]$/,/^\[/ s/^adminPassword=.*$/adminPassword=@@@@rocketchat.admin@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[mongodb\]$/,/^\[/ s/^mongoPassword=.*$/mongoPassword=@@@@rocketchat.db@@@@/' "$ADDON_VARS_INI"
                sed -i '/^\[mongodb\]$/,/^\[/ s/^mongoOplogPassword=.*$/mongoOplogPassword=@@@@rocketchat.oplogdb@@@@/' "$ADDON_VARS_INI"
            fi
            ;;
        wikijs)
            # Inject secret tokens
            if [ "$DISABLE_SECRETS_MANAGEMENT" = "NO" ]; then
                sed -i '/^\[wikijs\]$/,/^\[/ s/^#adminPassword=.*$/adminPassword=@@@@wikijs.admin@@@@/' "$ADDON_VARS_INI"
                #FIXME dbpassword is not customizable
                # sed -i '/^\[wikijs\]$/,/^\[/ s/^dbpassword=.*$/dbpassword=@@@@wikijs.db@@@@/' "$ADDON_VARS_INI"
            fi
            ;;
    esac
done