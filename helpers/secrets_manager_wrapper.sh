#!/usr/bin/env bash

# Exit script as soon as a command fails
set -e

# SCRIPT_DIR is equal to $CONF_DIR by construction
# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
CONF_DIR="$SCRIPT_DIR"

# Hardcoded
SECRETS_MANAGER_NAME='secrets_manager.py'
SECRETS_FILE_NAME='secrets.yaml'

# Must be updated with a real value
SECRETS_MANAGER="${CONF_DIR}/${SECRETS_MANAGER_NAME}"
SECRETS_FILE="${CONF_DIR}/${SECRETS_FILE_NAME}"

HELP_TEXT="Help: $0 [kubectl|helm|echo|echo_urlencoded] ARGS"

# Check we have a valid subcommand
if [[ $# -lt 1 ]]; then
    echo "You must provide at least one argument: kubectl, helm, echo or echo_urlencoded"
    echo "$HELP_TEXT"
    exit 1
fi
NEW_EXEC=$1 # Grab command either kubectl, helm or echo
if ! [[ "$NEW_EXEC" =~ ^(kubectl|helm|echo|echo_urlencoded)$ ]]; then
    echo "1st argument must be: kubectl, helm, echo or echo_urlencoded"
    echo "$HELP_TEXT"
    exit 1
fi
shift

# Fetch all remaining arguments
if [ "$NEW_EXEC" = "echo" ]; then
    NEW_ARGS=( "$@" )
else
    # Parse all arguments and rewrite all '-f filename.yaml' instances to only one '-f -'
    NEW_ARGS=()
    VALUES_FILES=()
    STDIN_DONE="NO"
    while [[ $# -gt 0 ]]; do
        case $1 in
            -f|--filename|--values)
                # Rewrite to take input from STDIN (only once)
                if [ "$STDIN_DONE" = "NO" ]; then
                    NEW_ARGS+=("$1" "-")
                    STDIN_DONE="YES"
                fi
                shift
                # Save filename to inject it through STDIN
                VALUES_FILES+=("$1")
                shift
                ;;
            *)
                NEW_ARGS+=("$1") # Save positional arg
                shift
                ;;
        esac
    done
fi

# Build the new command that integrates with the secrets management tool
# https://unix.stackexchange.com/questions/444946/how-can-we-run-a-command-stored-in-a-variable
if [ "$NEW_EXEC" = "echo" ]; then
    SUB_CMD=( "${SECRETS_MANAGER}" -s "${SECRETS_FILE}" process-string )
    NEW_CMD=( "${SUB_CMD[@]}" "${NEW_ARGS[@]}" )
    # echo "Rewritten command: ${NEW_CMD[*]}"
    "${NEW_CMD[@]}"
elif [ "$NEW_EXEC" = "echo_urlencoded" ]; then
    SUB_CMD=( "${SECRETS_MANAGER}" -s "${SECRETS_FILE}" process-string )
    NEW_CMD=( "${SUB_CMD[@]}" "${NEW_ARGS[@]}" )
    # echo_urlencoded "Rewritten command: ${NEW_CMD[*]}"
    "${NEW_CMD[@]}" | tr -d '\n' | jq -sRr @uri
else
    if [ ${#VALUES_FILES[@]} -gt 1 ]; then
        #FIXME If we have multiple values files check that the 2nd, 3rd, 4th and so on begin with'---' so that they can be concatenated
        # Check if the first non-empty line is equal to '---'
        # if [[ $(echo "$input" | sed -n '/^[[:space:]]*[^[:space:]]/ {p;q}') == '---' ]]; then
        #     echo "The first non-empty line is equal to '---'"
        # else
        #     echo "The first non-empty line is not equal to '---'"
        # fi
        echo "WARNING: Multiple values file detected"
    fi
    SUB_CMD=( "${SECRETS_MANAGER}" -s "${SECRETS_FILE}" process-file )
    NEW_CMD1=( "${SUB_CMD[@]}" "${VALUES_FILES[@]}" )
    NEW_CMD2=( "${NEW_EXEC}" "${NEW_ARGS[@]}")
    # echo "Rewritten command: ${NEW_CMD1[*]} | ${NEW_CMD2[*]}"
    "${NEW_CMD1[@]}" | "${NEW_CMD2[@]}"
fi
