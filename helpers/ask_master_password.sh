#!/usr/bin/env bash

# Be sure we are sourced :-)
# https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced
(return 0 2>/dev/null) && sourced=1 || sourced=0
if [ "$sourced" == "0" ]; then
    echo "ERROR: This script must be sourced to be able export variable 'SECRETS_MANAGER_MASTER_PASSWORD' variable"
    exit 1
fi

read -r -s -p "Enter your master password: " SECRETS_MANAGER_MASTER_PASSWORD
export SECRETS_MANAGER_MASTER_PASSWORD
echo
echo "Variable 'SECRETS_MANAGER_MASTER_PASSWORD' initialised with your master password and exported"
