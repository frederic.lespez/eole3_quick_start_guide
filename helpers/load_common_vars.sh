#!/usr/bin/env bash

# Be sure that tools and K8S cluster config will be found
KUBECONFIG="${SCRIPT_DIR}/${EOLE3_KUBECONFIG}"
PATH="${SCRIPT_DIR}/bin":$PATH
export KUBECONFIG
export PATH

# EOLE3 Domain
# shellcheck disable=SC2034
EOLE_DOMAIN="${CONF_NAME#env_}"

# Directory where is stored an environment's configuration
CONF_DIR="${SCRIPT_DIR}/${CONF_NAME}"

# Directory where are stored helpers
# shellcheck disable=SC2034
HELPERS_DIR="${SCRIPT_DIR}/helpers"

# Directory where EOLE3 code is stored (and will be checkout)
# shellcheck disable=SC2034
EOLE3_CODE_DIR="${SCRIPT_DIR}/tools"

# Directory where secrets manager code is stored (and will be checkout)
# shellcheck disable=SC2034
SECRETS_MANAGER_DIR="${SCRIPT_DIR}/secrets_manager"

# Directory where EOLE3 will generate installation files for environment
# shellcheck disable=SC2034
INSTALL_DIR="${CONF_DIR}/install"

# Filenames for domain's wilcard SSL certificate
# shellcheck disable=SC2034
WILDCARD_SSL_CERT='tls.crt'
# shellcheck disable=SC2034
WILDCARD_SSL_KEY='tls.key'

# CONF_DIR structure
# CONF_DIR
#    |
#    |_ secrets_manager.py
#    |_ secrets_manager_wrapper.sh
#    |_ secrets.yaml
