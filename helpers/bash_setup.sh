#!/usr/bin/env bash

# Be sure we are sourced :-)
# https://stackoverflow.com/questions/2683279/how-to-detect-if-a-script-is-being-sourced
(return 0 2>/dev/null) && sourced=1 || sourced=0
if [ "$sourced" == "0" ]; then
    echo "ERROR: This script must be sourced to be able to export 'PATH' and 'KUBECONFIG' variables"
    exit 1
fi

# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export PATH="$(dirname "$SCRIPT_DIR")/bin:${PATH}"
export KUBECONFIG="$(dirname "$SCRIPT_DIR")/kubeconfig.yaml"
source <(kubectl completion bash)
source <(helm completion bash)
