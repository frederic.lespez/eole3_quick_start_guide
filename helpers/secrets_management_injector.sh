#!/usr/bin/env bash

# Exit script as soon as a command fails
set -e

# Fetch arguments
CONF_DIR="$1"

# Hardcoded
SECRETS_MANAGER_WRAPPER='secrets_manager_wrapper.sh'

# Initialize secrets manager wrapper with the real path to the secrets manager
# sed -i -E "s|^SECRETS_MANAGER=.*$|SECRETS_MANAGER='""$SECRETS_MANAGER""'|" "${SECRETS_MANAGER_WRAPPER}"
# sed -i -E "s|^SECRETS_FILE=.*$|SECRETS_FILE='""$SECRETS_FILE""'|" "${SECRETS_MANAGER_WRAPPER}"

# Replace all kubectl and helm commands using value files by $KUBECTL or $HELM
# Replace all password assignations to a variable by a command subsitution using $ECHO (or $ECHO_URLENCODED)
echo "Injecting secrets management in configuration from folder '${CONF_DIR}'"
shopt -s extglob
for f in \
    "${CONF_DIR}/install/"!(*.yaml|*.json|tls.crt|tls.key|README|addons|admin-tools) \
    "${CONF_DIR}/install/addons/"*/!(*.yaml|*.json|README) \
    "${CONF_DIR}/install/admin-tools/"*/!(*.yaml|*.json|README); do
    if [[ ! -d "${CONF_DIR}/install/addons/" && $f == "${CONF_DIR}/install/addons/"* ]]; then
        echo "Ignore Addons"
        continue
    fi
    if [[ ! -d "${CONF_DIR}/install/admin-tools/" && $f == "${CONF_DIR}/install/admin-tools/"* ]]; then
        echo "Ignore Admin tools"
        continue
    fi
    # Build relative path toward secrets manager wrapper
    SECRETS_MANAGER_WRAPPER_PATH="../${SECRETS_MANAGER_WRAPPER}"
    if [[ $f == "${CONF_DIR}/install/addons/"* || $f == "${CONF_DIR}/install/admin-tools/"* ]]; then
        SECRETS_MANAGER_WRAPPER_PATH="../../../${SECRETS_MANAGER_WRAPPER}"
    fi
    echo "Patching: ${f}"
    # Do not add supercharged exports if they are already there
    if ! grep -Fxq '# Secrets injector declarations' "$f"; then
        sed -i '2 i \\' "$f"
        sed -i '2 i ECHO_URLENCODED="secrets_manager echo_urlencoded"' "$f"
        sed -i '2 i ECHO="secrets_manager echo"' "$f"
        sed -i '2 i HELM="secrets_manager helm"' "$f"
        sed -i '2 i KUBECTL="secrets_manager kubectl"' "$f"
        sed -i '2 i secrets_manager () { "'"$SECRETS_MANAGER_WRAPPER_PATH"'" "$@"; }' "$f"
        sed -i '2 i # Secrets injector declarations' "$f"
        sed -i '2 i \\' "$f"
    fi
    sed -i -E "s/^\s*kubectl(\s+(apply|delete))/\$KUBECTL\1/" "$f"
    sed -i -E "s/^\s*helm(\s+(install|upgrade|template))/\$HELM\1/" "$f"
    sed -i -E "s/^(\s*([a-zA-Z0-9_]+)=)(.@@@@([a-zA-Z0-9_./-]+)@@@@.)$/\1\$(\$ECHO \3)/" "$f"
    sed -i -E "s/^(\s*([a-zA-Z0-9_]+)=)(.)%40%40%40%40([a-zA-Z0-9_.%-]+)%40%40%40%40(.)$/\1\$(\$ECHO_URLENCODED \3@@@@\4@@@@\5)/" "$f"
    sed -i -E 's/^(\s*"secret":\s*)"@@@@([a-zA-Z0-9_.%-]+)@@@@",$/\1"\$(\$ECHO_URLENCODED "@@@@\2@@@@")",/' "$f"
    #FIXME We need to trap all of these for helm
    # --set stringArray         set values on the command line (can specify multiple or separate values with commas: key1=val1,key2=val2)
    # --set-file stringArray    set values from respective files specified via the command line (can specify multiple or separate values with commas: key1=path1,key2=path2)
    # --set-json stringArray    set JSON values on the command line (can specify multiple or separate values with commas: key1=jsonval1,key2=jsonval2)
    # --set-string stringArray  set STRING values on the command line (can specify multiple or separate values with commas: key1=val1,key2=val2)
    # What is the equivalent for HELM ?
    # install/update:$HELM upgrade -i -n laboite mongo-laboite bitnami/mongodb -f mongo-values.yaml --set auth.rootPassword='@@@@mongodb.root@@@@' --version "13.6.2"
    sed -i -E 's/(\s+--set\s+([a-zA-Z0-9_.]+)=)(.@@@@([a-zA-Z0-9_./-]+)@@@@.)/\1"\$(\$ECHO \3)"/' "$f"
done
shopt -u extglob
