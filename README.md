Quick Start Guide
=================

The goal of this document is to set up an EOLE environment for **TESTS ONLY** and guide you through a minimal configuration so you can play with the platform.

## Requirements

* A test server
  * Ubuntu 22.04
  * 4 CPU / 8Go
  * Disk Size minimum 40Go
  * Can be a VM
  * Should be on a private network. Do not expose on internet this test server for security reasons (passwords are trivial by default)
    * Its IP will be `SERVER_IP` in this guide
  * Must have access to internet
* A public domain with a wildcard record pointing to the server IP
  * Will use `mydomain.com` in this guide
  * Not necessarily dedicated (beware of conflicting records though!)
  * `anything.mydomain.org` must resolve to `SERVER_IP` if everything is setup properly
* A wildcard SSL certificate for this domain
  * Must be issued from a "public" CA (Let's Encrypt is fine)
* A desktop running Linux (should work in WSL)
  * With OpenSSH client installed
  * Must have access to internet
  * Root access on the test server through SSH
    * Prefer public key authentication
  * A regular user account with sudo's rights
    * Only used to install some packages from the distribution
  * A record in your `/etc/hosts` file named `k8s.eole3.lan` pointing to `SERVER_IP`

## Setup EOLE

On the desktop running Linux, logged as a regular user:
* create a new folder and put inside the scripts, the `config_k8s` and `service_logo.jpg` files. Check the scripts are executable
* Then "cd" into this folder and run these commands:

```bash
# Run once to install local tools
# You will be prompted by sudo for your password
./0_install_tools.sh

# Run once to deploy the Kubernetes cluster on the server
./1_deploy_k8s.sh

# [OPTIONAL] To use tools (helm, kubectl...) outside of the scripts
source helpers/bash_setup.sh

# Choose a master password
# This password will be used to encrypt/decrypt all secrets.
source helpers/ask_master_password.sh

# Run to generate a default configuration for the domain 'mydomain.com'
./2_gen_socle_for_domain.sh mydomain.com

# Put your wildcard SSL certificate files in place as asked by previous command

# Run to deploy EOLE3 core services from the configuration stored in the folder 'env_mydomain.com'
./4_deploy_socle.sh env_mydomain.com

# If your server is slow, wait for things to stabilize

# After checking that EOLE3 core services are working you can install addons
./3_gen_addons.sh env_mydomain.com codimd nextcloud
./5_deploy_addons.sh env_mydomain.com codimd nextcloud

# List all secrets
secrets_manager/secrets_manager.py -s env_mydomain.com/secrets.yaml display
```

## Using EOLE

1. Create an admin account
* Go to https://portail.mydomain.com
* Click on "SE CONNECTER", then on "Register"
* Fill in the form. The import field is "Email": You MUST use the email 'admin@mydomain.com'. This email doesn't have to exist, and no email we be sent to this address.
* Click on from the "Register" to create the account
* You will be redirected to the admin user's profile

2. Create a least a "Structure"
* From admin user's profile, in the menu (Top right corner, next to the bell icon), click on "Administration"
* Click on "Gestion des structures" and then add a structure (take any name you like)
* In the menu (Top right corner, next to the bell icon), click on "Profil" and choose a structure to finish the account setup

3. Create a least a "Service"
* From admin user's profile, in the menu (Top right corner, next to the bell icon), click on "Administration"
* Click on "Gestion des services" and then add a service
  * You must fill the fields below (You can fill the fields with anything you like except for the URL)
    * Titre
    * Équipe
    * Utilisation de l'application
    * Logo du service (that's why there is a `service_logo.png` file with this guide)
    * Courte description
    * URL du service
      * Here are some URLs you could use:
        * Agenda : https://agenda.ccheznous.org/
        * Blog : https://blog.ccheznous.org/
        * Mezig : https://mezig.ccheznous.org/
        * Sondage: https://sondage.ccheznous.org
        * CodiMD : https://codimd.mydomain.com
        * Nextcloud : https://nextcloud.mydomain.com
    * Longue description du service

Note that creating services is not mandatory. You can access them through their URL.

4. Logout admin user
* From admin user's profile, in the menu (top right corner, next to the bell icon), click on "Se déconnecter"

5. Create a user account
* Go to https://portail.mydomain.com
* Click on "SE CONNECTER", then on "Register"
* Fill in the form. The email doesn't have to exist, and no email we be sent to this address.
  * If you use an email from domain `mydomain.com`, the account will be automatically activated. Otherwise, the admin will have to validate the account.
* Click on from the "Register" to create the account
* You will be redirected to the user's profile and then you have to choose a structure to finish the account setup
* Now you can click on "Applications" (horizontal menu bar on top) and use EOLE platform's services :-)
