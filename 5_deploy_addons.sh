#!/usr/bin/env bash

# Exit script as soon as a command fails
set -e

# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Parse arguments
# https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
POSITIONAL_ARGS=()
UPDATE="NO"
DISABLE_SECRETS_MANAGEMENT="NO"
while [[ $# -gt 0 ]]; do
    case $1 in
        -u|--update)
            UPDATE="YES"
            shift # past argument
            ;;
        --disable-secrets-management)
            DISABLE_SECRETS_MANAGEMENT="YES"
            shift
            ;;
        -*)
            echo "Unknown option $1"
            exit 1
            ;;
        *)
            POSITIONAL_ARGS+=("$1") # save positional arg
            shift # past argument
            ;;
    esac
done
if (( ${#POSITIONAL_ARGS[@]} == 0 )); then
    echo "You must provide at least one argument: a folder name containing a configuration (example: 'env_mydomain.com')"
    echo "Help: $0 [--update] env_mydomain.com [addon_name1 [addon_name2 [...]]]"
    exit 1
fi
CONF_NAME=${POSITIONAL_ARGS[0]}
ADDON_LIST=("${POSITIONAL_ARGS[@]:1}")

# Get global config
source config_k8s

# Load common variables
source helpers/load_common_vars.sh

#
# Prepare deployment
#
for ADDON in "${ADDON_LIST[@]}"; do
    echo "Prepare addon '${ADDON}' deployment using configuration from folder '${CONF_DIR}'"

    # Generate installation files
    cd "$EOLE3_CODE_DIR"
    ./build \
        -c "${CONF_DIR}/vars.ini" \
        -cc "${CONF_DIR}/cluster-vars.ini" \
        -o "$INSTALL_DIR" \
        gen-addon \
        -n "${ADDON}" \
        -ac "${CONF_DIR}/${ADDON}-vars.ini"
done

#
# Enable secrets management
#

if [ "$DISABLE_SECRETS_MANAGEMENT" = "NO" ]; then
    echo "Enabling secrets management in configuration from folder '${INSTALL_DIR}'"
    # Copy secrets manager (tool & wrapper) into this environment's folder
    cp "${SECRETS_MANAGER_DIR}/secrets_manager.py" "${CONF_DIR}/secrets_manager.py"
    cp "${HELPERS_DIR}/secrets_manager_wrapper.sh" "${CONF_DIR}/secrets_manager_wrapper.sh"

    # Inject secrets management inside installation files for this environment
    "${HELPERS_DIR}/secrets_management_injector.sh" "$CONF_DIR"
fi

#
# Deployment
#

for ADDON in "${ADDON_LIST[@]}"; do
    # Deploy :-)
    cd "${INSTALL_DIR}/addons/${ADDON}"
    if [ "$UPDATE" = "NO" ]; then
        echo "Deploy addon '${ADDON}' using configuration from folder '${INSTALL_DIR}'"
        bash deploy
    else
        echo "Update addon '${ADDON}' using configuration from folder '${INSTALL_DIR}'"
        bash update
    fi
done
